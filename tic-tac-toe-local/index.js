let state = {
  data: [],
  move: [],
  currenHistoryIndex: 0,
  gameOver: false,
  winCells: [],
  winDirect: '',
  wonMessage: '',
  btnUndo: false,
  btnRedo: false,
  currentPlayerId: 1,
};

const players = {
  player1: {
    id: 1,
    class: 'ch',
    name: 'Potap',
  },
  player2: {
    id: 2,
    class: 'r',
    name: 'Nastya',
  },
};

const buttonUndo = document.querySelector('.undo-btn');
const buttonRedo = document.querySelector('.redo-btn');
const wonTitle = document.querySelector('.won-title');
const wonMessage = document.querySelector('.won-message');
const restartButton = document.querySelector('.restart-btn');

// localStorage.setItem('tictactoeState', JSON.stringify(state));

function addMove(cellData) {
  state.move = state.move.slice(0, state.currenHistoryIndex);
  state.move.push(cellData);
  state.currenHistoryIndex += 1;
}

function getCurrentMove() {
  return state.move[state.currenHistoryIndex - 1];
}

function isFirstMove() {
  return state.currenHistoryIndex === 0;
}

function isLastMove() {
  const len = state.move.length;
  return state.currenHistoryIndex === len - 1;
}

function switchPlayer() {
  if (state.currentPlayerId === players.player1.id) {
    state.currentPlayerId = players.player2.id;
  } else {
    state.currentPlayerId = players.player1.id;
  }
  localStorage.setItem('tictactoeState', JSON.stringify(state));
}

Reflect.defineProperty(this, 'btnUndo', {
  set(value) {
    state.btnUndo = value;
    buttonUndo.disabled = !value;
  },
  get() {
    return state.btnUndo;
  },
});

Reflect.defineProperty(this, 'btnRedo', {
  set(value) {
    state.btnRedo = value;
    buttonRedo.disabled = !value;
  },
  get() {
    return state.btnRedo;
  },
});

function getBoardDOM(fieldClass) {
  const fieldDOM = [];
  const fieldOfGame = document.querySelector(fieldClass);
  const fieldRows = fieldOfGame.childNodes;
  fieldRows.forEach(row => {
    const rowOfCells = [];
    const rowOfNulls = [];
    const fieldCells = row.childNodes;
    fieldCells.forEach(cell => {
      rowOfCells.push(cell);
      rowOfNulls.push(0);
    });
    fieldDOM.push(rowOfCells);
    state.data.push(rowOfNulls);
  });
  return fieldDOM;
}

function addListenerForCells(arr) {
  arr.forEach((row, rowIndex) => {
    row.forEach((cell, cellIndex) =>
      cell.addEventListener('click', e => {
        if (!state.gameOver) {
          const currentPlayer = players[`player${state.currentPlayerId}`];
          e.target.classList.add(currentPlayer.class);
          state.data[rowIndex][cellIndex] = state.currentPlayerId;
          addMove([rowIndex, cellIndex, currentPlayer.id]);
          btnUndo = true;
          btnRedo = false;
          isWin(state.data, rowIndex, cellIndex, currentPlayer.id);
          switchPlayer();
          localStorage.setItem('tictactoeState', JSON.stringify(state));
        }
      }),
    );
  });
}

const boardCells = getBoardDOM('.field');
addListenerForCells(boardCells);

function renderBoard(state, DOM, players) {
  DOM.forEach((row, r) => {
    row.forEach((cell, c) => {
      cell.classList.remove(players.player1.class);
      cell.classList.remove(players.player2.class);
      cell.classList.remove('win');
      cell.classList.remove('horizontal');
      cell.classList.remove('vertical');
      cell.classList.remove('diagonal-left');
      cell.classList.remove('diagonal-right');
      if (state.winDirect) {
        state.winCells.forEach(winCell => {
          const cellClassList = boardCells[winCell[0]][winCell[1]].classList;
          cellClassList.add('win');
          cellClassList.add(state.winDirect);
        });
      }
      if (state.data.length) {
        const cellData = state.data[r][c];
        if (cellData) {
          const playerClass = players[`player${cellData}`].class || '';
          cell.classList.add(playerClass);
        }
      }
    });
  });
}

function endGame() {
  state.gameOver = true;
  wonMessage.textContent = state.wonMessage;
  wonTitle.classList.remove('hidden');
  btnUndo = false;
  btnRedo = false;
  if (state.winDirect) {
    state.winCells.forEach(cell => {
      const cellClassList = boardCells[cell[0]][cell[1]].classList;
      cellClassList.add('win');
      cellClassList.add(state.winDirect);
    });
  }
}

function restartGame() {
  wonTitle.classList.add('hidden');
  state.move.length = 0;
  state.currenHistoryIndex = 0;
  boardCells.forEach((row, r) => {
    row.forEach((cell, c) => {
      state.data[r][c] = 0;
      cell.classList.remove(players.player1.class);
      cell.classList.remove(players.player2.class);
    });
  });
  state.currentPlayerId = players.player1.id;
  state.gameOver = false;
  if (state.winDirect) {
    state.winCells.forEach(cell => {
      const cellClassList = boardCells[cell[0]][cell[1]].classList;
      cellClassList.remove('win');
      cellClassList.remove(state.winDirect);
    });
  }
  state.winCells = [];
  state.winDirect = '';
  localStorage.setItem('tictactoeState', JSON.stringify(state));
}

function isWin(fieldData, lastMoveRow, lastMoveCol, playerId) {
  const horizontal = [];
  const vertical = [];
  const diagonalRight = [];
  const diagonalLeft = [];
  const len = fieldData.length;
  const fieldSize = fieldData.length * fieldData[0].length;

  // horizontal & vertical
  for (let i = 0; i < len; i += 1) {
    horizontal.push([lastMoveRow, i]);
    vertical.push([i, lastMoveCol]);
  }

  // diagonalRight & diagonalLeft
  for (let i = 0; i < len; i += 1) {
    diagonalRight.push([i, i]);
    diagonalLeft.push([i, len - i - 1]);
  }

  if (horizontal.every(cell => fieldData[cell[0]][cell[1]] === playerId)) {
    state.winCells = horizontal;
    state.winDirect = 'horizontal';
  }

  if (vertical.every(cell => fieldData[cell[0]][cell[1]] === playerId)) {
    state.winCells = vertical;
    state.winDirect = 'vertical';
  }

  if (diagonalRight.every(cell => fieldData[cell[0]][cell[1]] === playerId)) {
    state.winCells = diagonalRight;
    state.winDirect = 'diagonal-right';
  }

  if (diagonalLeft.every(cell => fieldData[cell[0]][cell[1]] === playerId)) {
    state.winCells = diagonalLeft;
    state.winDirect = 'diagonal-left';
  }

  if (state.winCells.length) {
    if (playerId === 1) {
      state.wonMessage = 'Crosses won!';
    } else {
      state.wonMessage = 'Toes won!';
    }
    endGame();
  } else if (state.move.length === fieldSize) {
    state.wonMessage = "It's a draw!";
    endGame();
  }
}

window.addEventListener('storage', e => {
  if (e.key === 'tictactoeState') {
    state = JSON.parse(e.newValue);
    buttonUndo.disabled = !btnUndo;
    buttonRedo.disabled = !btnRedo;
    wonTitle.classList.add('hidden');
    renderBoard(state, boardCells, players);
    if (state.gameOver) {
      endGame();
    }
  }
});

buttonUndo.addEventListener('click', () => {
  const currentNodeRow = getCurrentMove()[0];
  const currentNodeCol = getCurrentMove()[1];
  const playerId = getCurrentMove()[2];
  const palyer = players[`player${playerId}`];
  const cell = boardCells[currentNodeRow][currentNodeCol];
  state.data[currentNodeRow][currentNodeCol] = 0;
  cell.classList.remove(palyer.class);
  if (!isLastMove()) {
    btnRedo = true;
  }
  state.currenHistoryIndex -= 1;
  if (isFirstMove()) {
    btnUndo = false;
  }
  switchPlayer();
  localStorage.setItem('tictactoeState', JSON.stringify(state));
});

buttonRedo.addEventListener('click', () => {
  if (isLastMove()) {
    btnRedo = false;
  }
  state.currenHistoryIndex += 1;
  const currentNodeRow = getCurrentMove()[0];
  const currentNodeCol = getCurrentMove()[1];
  const playerId = getCurrentMove()[2];
  const palyer = players[`player${playerId}`];
  const cell = boardCells[currentNodeRow][currentNodeCol];
  state.data[currentNodeRow][currentNodeCol] = playerId;
  cell.classList.add(palyer.class);
  switchPlayer();
  btnUndo = true;
  localStorage.setItem('tictactoeState', JSON.stringify(state));
});

restartButton.addEventListener('click', () => restartGame());

function appInit() {
  const currentState = JSON.parse(localStorage.getItem('tictactoeState'));
  if (currentState) {
    state = currentState;
    buttonUndo.disabled = !btnUndo;
    buttonRedo.disabled = !btnRedo;
    wonTitle.classList.add('hidden');
    renderBoard(state, boardCells, players);
    if (state.gameOver) {
      endGame();
    }
  } else {
    localStorage.setItem('tictactoeState', JSON.stringify(state));
  }
}

document.addEventListener('DOMContentLoaded', () => appInit());
